package com.inter.tulio.Desafio.Inter.WebApi;

import com.inter.tulio.Desafio.Inter.Dto.PedidoCompraAcaoDto;
import com.inter.tulio.Desafio.Inter.Dto.TransacaoDto;
import com.inter.tulio.Desafio.Inter.Service.TransacaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transacao")
@Api("Api Rest Transacao")
public class TransacaoController {
    @Autowired
    TransacaoService transacaoService;

    @PostMapping
    @ApiOperation(value="Investir em um grupo de acoes, retorna lista de acoes investidas")
    public TransacaoDto comprarAcaoPorEmpresaValor(@RequestBody PedidoCompraAcaoDto pedidoCompra) throws Exception {
        return transacaoService.efetuarCompraAcao(pedidoCompra.getCpf(), pedidoCompra.getQuantidadeEmpresas(), pedidoCompra.getValorTotal());
    }

    @GetMapping("/historico/{cpf}")
    @ApiOperation(value="Obter lista de acoes investidas")
    public List<TransacaoDto> obterInvestimentosRealizados (@PathVariable String cpf) throws Exception {
        return transacaoService.obterInvestimentosRealizados(cpf);
    }
}