package com.inter.tulio.Desafio.Inter.Service;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;

import java.util.List;

public interface EmpresaService {

    Empresa inserirEmpresa (Empresa empresa);
    Empresa obterEmpresaPorId (long id);
    List<Empresa> obterEmpresaPorStatus (int status);
    List<Empresa> obterTodasEmpresas ();
    void excluirEmpresa (long id);
    void atualizarPrecoAcaoEmpresaPorId (long empresaId, double valor);
    void atualizarStatusEmpresaPorId ( long empresaId, int status);

}