package com.inter.tulio.Desafio.Inter.Repository;

import com.inter.tulio.Desafio.Inter.Entity.ItemTransacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ItemTransacaoRepositorio extends JpaRepository<ItemTransacao, Long> {

    List<ItemTransacao> findByTransacaoId (@Param("transacao_id") Long transacaoId);
}

