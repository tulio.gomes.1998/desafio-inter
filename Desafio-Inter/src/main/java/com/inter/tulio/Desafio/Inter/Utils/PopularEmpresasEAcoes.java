package com.inter.tulio.Desafio.Inter.Utils;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Repository.EmpresaRepositorio;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PopularEmpresasEAcoes  implements InitializingBean {

    @Autowired
    EmpresaRepositorio empresaRepositorio;

    @Override
    public void afterPropertiesSet() throws Exception {
        Empresa empresa1 = new Empresa();
        empresa1.setNome("Inter");
        empresa1.setTicker("BIDI11");
        empresa1.setAtiva(1);
        empresa1.setValor(66.51);
        empresaRepositorio.save(empresa1);

        Empresa empresa2 = new Empresa();
        empresa2.setNome("Magazine Luiza");
        empresa2.setTicker("MGLU3");
        empresa2.setAtiva(1);
        empresa2.setValor(18.8);
        empresaRepositorio.save(empresa2);

        Empresa empresa3 = new Empresa();
        empresa3.setNome("Sulamérica");
        empresa3.setTicker("SULA11");
        empresa3.setAtiva(1);
        empresa3.setValor(28.26);
        empresaRepositorio.save(empresa3);

        Empresa empresa4 = new Empresa();
        empresa4.setNome("Engie");
        empresa4.setTicker("EGIE3");
        empresa4.setAtiva(1);
        empresa4.setValor(38.30);
        empresaRepositorio.save(empresa4);

        Empresa empresa5 = new Empresa();
        empresa5.setNome("CVC");
        empresa5.setTicker("CVCB3");
        empresa5.setAtiva(1);
        empresa5.setValor(20.87);
        empresaRepositorio.save(empresa5);

        Empresa empresa6 = new Empresa();
        empresa6.setNome("Renner");
        empresa6.setTicker("LREN3");
        empresa6.setAtiva(1);
        empresa6.setValor(36.95);
        empresaRepositorio.save(empresa6);

        Empresa empresa7 = new Empresa();
        empresa7.setNome("Marisa");
        empresa7.setTicker("AMAR3");
        empresa7.setAtiva(1);
        empresa7.setValor(6.3);
        empresaRepositorio.save(empresa7);
    }
}