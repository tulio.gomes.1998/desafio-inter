package com.inter.tulio.Desafio.Inter.WebApi;
import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Service.EmpresaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/empresa")
@RestController
@Api("Api Rest Empresa")
public class EmpresaController {
    @Autowired
    EmpresaService empresaService;

    @GetMapping
    @ApiOperation(value="Obtem lista de empresas com valor de acoes")
    public List<Empresa> obterTodasEmpresas () {
        return empresaService.obterTodasEmpresas();
    }

    @GetMapping("/{id}")
    @ApiOperation(value="Obtem empresa por empresaId")
    public Empresa obterEmpresaPorId (@PathVariable long id) {
        return empresaService.obterEmpresaPorId(id);
    }

    @GetMapping("/status/{status}")
    @ApiOperation(value="Obtem lista empresa por status, 1==ativo 0==inativo")
    public List<Empresa> obterEmpresaPorId (@PathVariable int status) {
        return empresaService.obterEmpresaPorStatus(status);
    }

    @PostMapping
    @ApiOperation(value="Salva nova empresa")
    public Empresa criarEmpresas (@RequestBody Empresa empresa) {
        return empresaService.inserirEmpresa(empresa);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value="Deleta uma empresa")
    public void excluirEmpresa (@PathVariable long id) {
        empresaService.excluirEmpresa(id);
    }

    @RequestMapping(value = "/{empresaId}/{valor}/atualizarvalor", method = RequestMethod.PUT)
    @ApiOperation(value="Atualiza valor de acao de uma empresa por id")
    public void atualizarPrecoAcoes ( @PathVariable("empresaId") long empresaId, @PathVariable("valor") double valor) {
         empresaService.atualizarPrecoAcaoEmpresaPorId(empresaId, valor);
    }

    @RequestMapping(value = "/{empresaId}/{status}/atualizarstatus", method = RequestMethod.PUT)
    @ApiOperation(value="Atualiza status de acao de uma empresa por id")
    public void atualizarStatusEmpresas ( @PathVariable("empresaId") long empresaId, @PathVariable("status") int status) {
        empresaService.atualizarStatusEmpresaPorId(empresaId, status);
    }
}
