package com.inter.tulio.Desafio.Inter.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemTransacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long itemTransacaoId;
    @Column(name = "transacaoId")
    long transacaoId;
    @Column(name = "empresaId")
    long empresaId;
    @Column(name = "quantidade")
    int quantidade;
    @Column(name = "valorAcao")
    double valorAcao;
    @Column(name = "valorTotal")
    double valorTotal;
}