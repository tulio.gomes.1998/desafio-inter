package com.inter.tulio.Desafio.Inter.Repository;

import com.inter.tulio.Desafio.Inter.Entity.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransacaoRepositorio extends JpaRepository<Transacao, Long> {

    List<Transacao> findByCpf (@Param("cpf") String cpf);
}
