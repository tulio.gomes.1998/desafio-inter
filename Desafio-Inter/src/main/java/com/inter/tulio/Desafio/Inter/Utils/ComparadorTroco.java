package com.inter.tulio.Desafio.Inter.Utils;

import com.inter.tulio.Desafio.Inter.Dto.ItemTransacaoDto;

import java.util.Comparator;

public class ComparadorTroco implements Comparator<ItemTransacaoDto> {
    @Override
    public int compare(ItemTransacaoDto empresaComp1, ItemTransacaoDto empresaComp2 ){
        if (empresaComp1.getTrocoItemTransacao() >= empresaComp2.getTrocoItemTransacao())
            return 1;
        return -1;
    }
}
