package com.inter.tulio.Desafio.Inter.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long empresaId;
    @Column(name = "nome")
    String nome;
    @Column(name = "ticker")
    String ticker;
    @Column(name = "valor")
    Double valor;
    @Column(name = "ativa")
    Integer ativa;
}