package com.inter.tulio.Desafio.Inter.Service.impl;

import com.inter.tulio.Desafio.Inter.Dto.ItemTransacaoDto;
import com.inter.tulio.Desafio.Inter.Dto.TransacaoDto;
import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Entity.ItemTransacao;
import com.inter.tulio.Desafio.Inter.Entity.Transacao;
import com.inter.tulio.Desafio.Inter.Repository.ItemTransacaoRepositorio;
import com.inter.tulio.Desafio.Inter.Repository.TransacaoRepositorio;
import com.inter.tulio.Desafio.Inter.Service.EmpresaService;
import com.inter.tulio.Desafio.Inter.Service.TransacaoService;
import com.inter.tulio.Desafio.Inter.Utils.ComparadorAcoes;
import com.inter.tulio.Desafio.Inter.Utils.ComparadorTroco;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Service
public class TransacaoServiceImpl implements TransacaoService {

    @Autowired
    private TransacaoRepositorio transacaoRepositorio;
    @Autowired
    private ItemTransacaoRepositorio itemTransacaoRepositorio;
    @Autowired
    private EmpresaService empresaService;
    @Autowired
    private ModelMapper modelMapper;

    public TransacaoServiceImpl(TransacaoRepositorio transacaoRepositorio, EmpresaService empresaService, ModelMapper modelMapper, ItemTransacaoRepositorio itemTransacaoRepositorio) {
        this.transacaoRepositorio = transacaoRepositorio;
        this.itemTransacaoRepositorio = itemTransacaoRepositorio;
        this.empresaService = empresaService;
        this.modelMapper = modelMapper;
    }

    public List<TransacaoDto> obterInvestimentosRealizados (String cpf) throws Exception {
        if (cpf == null || cpf.length() != 11) {
            throw new Exception("parametros invalidos");
        }
        List<Transacao> transacao = transacaoRepositorio.findByCpf(cpf);
        List<TransacaoDto> listaTransacaoDto = new ArrayList<>();

        for (Transacao item : transacao){
            TransacaoDto transacaoDto = modelMapper.map(item, TransacaoDto.class);
            List<ItemTransacaoDto> listaItemTransacaoDto = new ArrayList<>();
            List<ItemTransacao> listaItemTransacao = itemTransacaoRepositorio.findByTransacaoId(item.getTransacaoId());
            for (ItemTransacao itemTransacao : listaItemTransacao) {
                ItemTransacaoDto itemTransacaoDto = new ItemTransacaoDto();
                itemTransacaoDto.setItemTransacaoId(itemTransacao.getItemTransacaoId());
                itemTransacaoDto.setQuantidadeAcoesCompradas(itemTransacao.getQuantidade());
                itemTransacaoDto.setValorAcaoEmpresa(itemTransacao.getValorAcao());
                itemTransacaoDto.setValorTotalEmpresa(itemTransacao.getValorTotal());
                itemTransacaoDto.setEmpresaId(itemTransacao.getEmpresaId());
                listaItemTransacaoDto.add(itemTransacaoDto);
            }
            transacaoDto.setAcoesCompradas(listaItemTransacaoDto);
            transacaoDto.setValorTotal(item.getValor());
            listaTransacaoDto.add(transacaoDto);
        }
        return listaTransacaoDto;
    }

    public TransacaoDto efetuarCompraAcao (String cpf, int quantidadeEmpresa, double valor ) throws Exception {
        if (cpf == null || cpf.length() != 11 || valor < 0 || quantidadeEmpresa < 0) {
            throw new Exception("parametros invalidos");
        }
        List<Empresa> empresasAtivas = empresaService.obterEmpresaPorStatus(1);
        Collections.sort(empresasAtivas, new ComparadorAcoes());
        TransacaoDto transacao = GerarItensTransacao(empresasAtivas, quantidadeEmpresa, cpf, valor);
        transacao.setCpf(cpf);
        transacao.setQuantidadeEmpresas(quantidadeEmpresa);


        Transacao transacaoEntity = modelMapper.map(transacao, Transacao.class);
        transacaoEntity.setTroco(transacao.getTroco());
        transacaoEntity.setValor(transacao.getValorTotal());
        transacaoEntity = transacaoRepositorio.save(transacaoEntity);
        transacao.setTransacaoId(transacaoEntity.getTransacaoId());
        for( ItemTransacaoDto item: transacao.getAcoesCompradas()) {
            ItemTransacao itemTransacaoEntidade = new ItemTransacao();
            itemTransacaoEntidade.setTransacaoId(transacaoEntity.getTransacaoId());
            itemTransacaoEntidade.setQuantidade(item.getQuantidadeAcoesCompradas());
            itemTransacaoEntidade.setValorAcao(item.getValorAcaoEmpresa());
            itemTransacaoEntidade.setValorTotal(item.getValorTotalEmpresa());
            itemTransacaoEntidade.setEmpresaId(item.getEmpresaId());
            ItemTransacao save = itemTransacaoRepositorio.save(itemTransacaoEntidade);
            item.setItemTransacaoId(save.getItemTransacaoId());
        }

        return transacao;
    }

    private TransacaoDto GerarItensTransacao(List<Empresa> empresasAtivas, int quantidadeEmpresas, String cpf, double valorOrcamento) {
        double valorPorEmpresa = valorOrcamento/quantidadeEmpresas;
        List<ItemTransacaoDto> possiveisAcoes = new ArrayList();
        for (Empresa empreasAtual : empresasAtivas) {
            double quantidadePossivel = valorPorEmpresa/empreasAtual.getValor();
            double trocoRestande = valorPorEmpresa%empreasAtual.getValor();
            if (trocoRestande >= empreasAtual.getValor()/2) {
                quantidadePossivel++;
            }
            ItemTransacaoDto pedidoDeCompraAcao = GerarPossibilidadesCompraAcoes(empreasAtual, (int)quantidadePossivel, valorPorEmpresa);
            possiveisAcoes.add(pedidoDeCompraAcao);
        }
        return FiltrarAcoesParaMinimizarTroco(possiveisAcoes, quantidadeEmpresas, valorOrcamento);
    }

    private TransacaoDto FiltrarAcoesParaMinimizarTroco(List<ItemTransacaoDto> possiveisAcoes, int quantidadeEmpresas, double valorOrcamento) {
        List<ItemTransacaoDto> listaAcoesParaCompra = new ArrayList();
        for (ItemTransacaoDto empreasAtual : possiveisAcoes) {
            empreasAtual.setTrocoItemTransacao( Math.abs(empreasAtual.getTrocoItemTransacao()));
        }
        listaAcoesParaCompra.add(possiveisAcoes.get(possiveisAcoes.size()-1));
        double valorTotalCompra = possiveisAcoes.get(possiveisAcoes.size()-1).getValorTotalEmpresa();
        possiveisAcoes.remove(possiveisAcoes.size()-1);
        Collections.sort(possiveisAcoes, new ComparadorTroco());
        for (int i = 0; i < quantidadeEmpresas-1; i++) {
            listaAcoesParaCompra.add(possiveisAcoes.get(i));
            valorTotalCompra += possiveisAcoes.get(i).getValorTotalEmpresa();
        }
        return AjustarListaParaCompra(listaAcoesParaCompra, valorOrcamento, valorTotalCompra);
    }

    private TransacaoDto AjustarListaParaCompra(List<ItemTransacaoDto> listaAcoesParaCompra, double valorOrcamento, double valorTotalCompra) {
        TransacaoDto transacaoDto = new TransacaoDto();
        double valorRestante = valorOrcamento - valorTotalCompra;
        ItemTransacaoDto menorAcao = listaAcoesParaCompra.stream().findFirst().get();
        double valorAcao = menorAcao.getValorAcaoEmpresa();
        double valorCompraExtra = ((int)(valorRestante/valorAcao) * valorAcao);
        if (valorRestante < 0) {
            if (valorCompraExtra == 0) {
                menorAcao.setQuantidadeAcoesCompradas(menorAcao.getQuantidadeAcoesCompradas() -1 );
                menorAcao.setValorTotalEmpresa(menorAcao.getValorTotalEmpresa() - valorAcao);
                valorCompraExtra = 0-valorAcao;
            }else {
                menorAcao.setQuantidadeAcoesCompradas(menorAcao.getQuantidadeAcoesCompradas() + (int)(valorRestante/valorAcao) + (-1));
                menorAcao.setValorTotalEmpresa(menorAcao.getValorTotalEmpresa() + valorCompraExtra + (-valorAcao));
                valorCompraExtra = valorCompraExtra + (-valorAcao);
            }

        } else {
            menorAcao.setQuantidadeAcoesCompradas(menorAcao.getQuantidadeAcoesCompradas() + (int)(valorRestante/valorAcao));
            menorAcao.setValorTotalEmpresa(menorAcao.getValorTotalEmpresa() + valorCompraExtra);
        }
        listaAcoesParaCompra.set(0, menorAcao);
        transacaoDto.setAcoesCompradas(listaAcoesParaCompra);
        transacaoDto.setValorTotal(valorTotalCompra + valorCompraExtra);
        transacaoDto.setTroco(valorRestante - valorCompraExtra);
        return transacaoDto;
    }

    private ItemTransacaoDto GerarPossibilidadesCompraAcoes (Empresa empreasAtual, int quantidade, double valorPorEmpresa) {
        ItemTransacaoDto item = new ItemTransacaoDto();
        item.setValorAcaoEmpresa(empreasAtual.getValor());
        item.setQuantidadeAcoesCompradas(quantidade);
        item.setValorTotalEmpresa(quantidade * empreasAtual.getValor());
        item.setTrocoItemTransacao(valorPorEmpresa-(quantidade * empreasAtual.getValor()));
        item.setEmpresaId(empreasAtual.getEmpresaId());
        return item;
    }

}
