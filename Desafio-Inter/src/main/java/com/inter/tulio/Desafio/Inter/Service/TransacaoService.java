package com.inter.tulio.Desafio.Inter.Service;

import com.inter.tulio.Desafio.Inter.Dto.TransacaoDto;
import java.util.List;

public interface TransacaoService {
    TransacaoDto efetuarCompraAcao (String cpf, int quantidadeEmpresa, double valor ) throws Exception;
    List<TransacaoDto> obterInvestimentosRealizados(String cpf) throws Exception;
}
