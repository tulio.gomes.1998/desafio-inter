package com.inter.tulio.Desafio.Inter.Dto;

import lombok.Data;

@Data
public class ItemTransacaoDto {
    private Long itemTransacaoId;
    private int quantidadeAcoesCompradas;
    private Double valorAcaoEmpresa;
    private Double valorTotalEmpresa;
    private Double trocoItemTransacao;
    private Long empresaId;
}