package com.inter.tulio.Desafio.Inter.Repository;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmpresaRepositorio extends JpaRepository<Empresa, Long>{

    @Modifying
    @Query("update Empresa e set e.valor = :valor where e.empresaId = :empresaId")
    void atualizarPrecoAcao(@Param(value = "empresaId") long empresaId, @Param(value = "valor") double valor);

    @Modifying
    @Query("update Empresa e set e.ativa = :ativa where e.empresaId = :empresaId")
    void atualizarStatusEmpresa(@Param(value = "empresaId") long empresaId, @Param(value = "ativa") int ativa);

    List<Empresa> findByAtiva (@Param("ativa") int ativa);
}
