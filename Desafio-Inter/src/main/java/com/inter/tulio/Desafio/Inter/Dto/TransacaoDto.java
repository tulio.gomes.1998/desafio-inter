package com.inter.tulio.Desafio.Inter.Dto;

import lombok.Data;

import java.util.List;

@Data
public class TransacaoDto {
        private Long transacaoId;
        private String cpf;
        private int quantidadeEmpresas;
        private Double valorTotal;
        private List<ItemTransacaoDto> acoesCompradas;
        private Double troco;
}
