package com.inter.tulio.Desafio.Inter.Dto;

import lombok.Data;

@Data
public class PedidoCompraAcaoDto {
    private String cpf;
    private int quantidadeEmpresas;
    private Double valorTotal;
}