package com.inter.tulio.Desafio.Inter.Service.impl;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Repository.EmpresaRepositorio;
import com.inter.tulio.Desafio.Inter.Service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class EmpresaServiceImpl implements EmpresaService {

    @Autowired
    private EmpresaRepositorio empresaRepositorio;
    public EmpresaServiceImpl(EmpresaRepositorio empresaRepositorio) {
        this.empresaRepositorio = empresaRepositorio;
    }
    public Empresa inserirEmpresa (Empresa empresa) {
        return empresaRepositorio.save(empresa);
    }

    public Empresa obterEmpresaPorId (long id) {
        if (!empresaRepositorio.findById(id).isPresent())
            return null;

        return empresaRepositorio.getById(id);
    }

    public List<Empresa> obterEmpresaPorStatus (int status) {
        if (status != 0 && status != 1)
            return null;

        return empresaRepositorio.findByAtiva(status);
    }

    public List<Empresa> obterTodasEmpresas () {
        return empresaRepositorio.findAll();
    }

    public void excluirEmpresa (long id) {
        empresaRepositorio.deleteById(id);
    }

    @Transactional
    public void atualizarPrecoAcaoEmpresaPorId ( long empresaId, double valor) {
        if (valor < 0) return;
        empresaRepositorio.atualizarPrecoAcao(empresaId, valor);
    }

    @Transactional
    public void atualizarStatusEmpresaPorId ( long empresaId, int status) {
        if (status != 0 && status != 1) return;
        empresaRepositorio.atualizarStatusEmpresa(empresaId, status);
    }
}