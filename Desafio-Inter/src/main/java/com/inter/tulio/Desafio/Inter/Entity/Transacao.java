package com.inter.tulio.Desafio.Inter.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transacao {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long transacaoId;
    @Column(name = "cpf")
    String cpf;
    @Column(name = "dataTransacao")
    LocalDateTime dataTransacao;
    @Column(name = "valor")
    double valor;
    @Column(name = "quantidadeEmpresas")
    int quantidadeEmpresas;
    @Column(name = "troco")
    double troco;

}
