package com.inter.tulio.Desafio.Inter.Utils;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;

import java.util.Comparator;

public class ComparadorAcoes implements Comparator<Empresa> {
    @Override
    public int compare(Empresa empresaComp1, Empresa empresaComp2 ){
        if (empresaComp1.getValor() <= empresaComp2.getValor())
            return 1;
        return -1;
    }
}
