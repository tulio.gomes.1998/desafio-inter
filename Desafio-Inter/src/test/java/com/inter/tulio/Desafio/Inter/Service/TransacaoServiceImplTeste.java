package com.inter.tulio.Desafio.Inter.Service;

import com.inter.tulio.Desafio.Inter.Dto.TransacaoDto;
import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Entity.ItemTransacao;
import com.inter.tulio.Desafio.Inter.Entity.Transacao;
import com.inter.tulio.Desafio.Inter.Repository.ItemTransacaoRepositorio;
import com.inter.tulio.Desafio.Inter.Repository.TransacaoRepositorio;
import com.inter.tulio.Desafio.Inter.Service.impl.TransacaoServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@Import(TransacaoServiceImpl.class)
@MockBeans({ @MockBean(TransacaoServiceImpl.class), @MockBean(TransacaoRepositorio.class), @MockBean(ItemTransacaoRepositorio.class) , @MockBean(EmpresaService.class) , @MockBean(ModelMapper.class)})
public class TransacaoServiceImplTeste {

    @MockBean
    private TransacaoRepositorio transacaoRepositorio = mock(TransacaoRepositorio.class);
    @MockBean
    private ItemTransacaoRepositorio itemTransacaoRepositorio = mock(ItemTransacaoRepositorio.class);
    @MockBean
    private EmpresaService empresaService = mock(EmpresaService.class);
    @MockBean
    private ModelMapper modelMapper = mock(ModelMapper.class);

    @Autowired
    private TransacaoServiceImpl serviceImp = new TransacaoServiceImpl(transacaoRepositorio, empresaService, modelMapper, itemTransacaoRepositorio);

    @Test
    public void comprarAcoesSucesso() throws Exception {
        String cpf = "12345678901";
        int quantidadeEmpresa = 4;
        double valorOrcamento = 1000;


        List<Empresa> listEmpresaMock = new ArrayList<Empresa>();
        for (int i =1; i <= 7; i++) {
            Empresa empresaMock = new Empresa();
            empresaMock.setEmpresaId((long)i);
            empresaMock.setAtiva(1);
            empresaMock.setValor(36.95 + i*2);
            empresaMock.setTicker("teste");
            empresaMock.setNome("teste");
            listEmpresaMock.add(empresaMock);
        }

        Transacao transacaoEntity = new Transacao();
        Transacao transacaoEntityPersistida = new Transacao();
        transacaoEntityPersistida.setTransacaoId(1);
        Mockito.when(empresaService.obterEmpresaPorStatus(1)).thenReturn(listEmpresaMock);
        Mockito.when(modelMapper.map(any(),any())).thenReturn(transacaoEntity);
        Mockito.when(transacaoRepositorio.save(transacaoEntity)).thenReturn(transacaoEntityPersistida);
        Mockito.when(itemTransacaoRepositorio.save(any())).thenReturn(new ItemTransacao());

        TransacaoDto transacaoDto = serviceImp.efetuarCompraAcao(cpf, quantidadeEmpresa, valorOrcamento);

        assertNotNull(transacaoDto);
        assertEquals(quantidadeEmpresa, transacaoDto.getAcoesCompradas().size());
        Mockito.verify(empresaService, times(1)).obterEmpresaPorStatus(1);
        Mockito.verify(modelMapper, times(1)).map(any(),any());
        Mockito.verify(transacaoRepositorio, times(1)).save(transacaoEntity);
        Mockito.verify(itemTransacaoRepositorio, times(4)).save(any());
        Mockito.verifyNoMoreInteractions(empresaService);
    }

    @Test
    public void comprarAcoesParametroInvalido() throws Exception {
        String cpf = "12345678";
        int quantidadeEmpresa = 4;
        double valorOrcamento = 1000;


        List<Empresa> listEmpresaMock = new ArrayList<Empresa>();
        for (int i =1; i <= 7; i++) {
            Empresa empresaMock = new Empresa();
            empresaMock.setEmpresaId((long)i);
            empresaMock.setAtiva(1);
            empresaMock.setValor(36.95 + i*2);
            empresaMock.setTicker("teste");
            empresaMock.setNome("teste");
            listEmpresaMock.add(empresaMock);
        }

        Transacao transacaoEntity = new Transacao();
        Transacao transacaoEntityPersistida = new Transacao();
        transacaoEntityPersistida.setTransacaoId(1);
        Mockito.when(empresaService.obterEmpresaPorStatus(1)).thenReturn(listEmpresaMock);
        Mockito.when(modelMapper.map(any(),any())).thenReturn(transacaoEntity);
        Mockito.when(transacaoRepositorio.save(transacaoEntity)).thenReturn(transacaoEntityPersistida);
        Mockito.when(itemTransacaoRepositorio.save(any())).thenReturn(new ItemTransacao());


        Exception thrown = assertThrows(Exception.class, () -> {
            TransacaoDto transacaoDto = serviceImp.efetuarCompraAcao(cpf, quantidadeEmpresa, valorOrcamento);
        });
    }

    @Test
    public void listarTransacoes() throws Exception {
        String cpf = "12345678901";

        List<Transacao> listTransacaoMock = new ArrayList<Transacao>();
        Transacao transacaoMock = new Transacao();
        transacaoMock.setTransacaoId((long)1);
        transacaoMock.setValor(999.0000);
        transacaoMock.setCpf(cpf);
        transacaoMock.setQuantidadeEmpresas(4);
        listTransacaoMock.add(transacaoMock);

        List<ItemTransacao> listItemTransacaoMock = new ArrayList<ItemTransacao>();
        for (int i =1; i <= 4; i++) {
            ItemTransacao itemTransacaoMock = new ItemTransacao();
            itemTransacaoMock.setItemTransacaoId((long)i);
            itemTransacaoMock.setQuantidade(1);
            itemTransacaoMock.setValorAcao(249.75);
            itemTransacaoMock.setEmpresaId(1);
            itemTransacaoMock.setValorTotal(249.75);
            listItemTransacaoMock.add(itemTransacaoMock);
        }
        TransacaoDto transacaoDto = new TransacaoDto();
        transacaoDto.setTransacaoId((long)1);
        transacaoDto.setValorTotal(999.0000);
        transacaoDto.setCpf(cpf);
        transacaoDto.setQuantidadeEmpresas(4);

        Mockito.when(transacaoRepositorio.findByCpf(cpf)).thenReturn(listTransacaoMock);
        Mockito.when(itemTransacaoRepositorio.findByTransacaoId((long)1)).thenReturn(listItemTransacaoMock);
        Mockito.when(modelMapper.map(any(),any())).thenReturn(transacaoDto);

        List<TransacaoDto> listTransacaoDto = serviceImp.obterInvestimentosRealizados(cpf);

        assertNotNull(listTransacaoDto);
        assertNotNull(listTransacaoDto.stream().findFirst().get().getAcoesCompradas());


        Mockito.verify(modelMapper, times(1)).map(any(),any());
        Mockito.verify(transacaoRepositorio, times(1)).findByCpf(cpf);
        Mockito.verify(itemTransacaoRepositorio, times(1)).findByTransacaoId((long)1);
        Mockito.verifyNoMoreInteractions(empresaService);
    }

    @Test
    public void listarTransacoesInvalido() throws Exception {
        String cpf = "12345678";

        List<Empresa> listEmpresaMock = new ArrayList<Empresa>();
        for (int i =1; i <= 7; i++) {
            Empresa empresaMock = new Empresa();
            empresaMock.setEmpresaId((long)i);
            empresaMock.setAtiva(1);
            empresaMock.setValor(36.95 + i*2);
            empresaMock.setTicker("teste");
            empresaMock.setNome("teste");
            listEmpresaMock.add(empresaMock);
        }

        Transacao transacaoEntity = new Transacao();
        Transacao transacaoEntityPersistida = new Transacao();
        transacaoEntityPersistida.setTransacaoId(1);

        Mockito.when(modelMapper.map(any(),any())).thenReturn(transacaoEntity);
        Mockito.when(transacaoRepositorio.save(transacaoEntity)).thenReturn(transacaoEntityPersistida);
        Mockito.when(itemTransacaoRepositorio.save(any())).thenReturn(new ItemTransacao());

        Exception thrown = assertThrows(Exception.class, () -> {
            List<TransacaoDto> transacaoDto = serviceImp.obterInvestimentosRealizados(cpf);
        });
    }
}