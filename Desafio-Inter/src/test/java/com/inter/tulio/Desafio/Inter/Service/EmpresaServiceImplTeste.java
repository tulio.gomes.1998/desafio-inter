package com.inter.tulio.Desafio.Inter.Service;

import com.inter.tulio.Desafio.Inter.Entity.Empresa;
import com.inter.tulio.Desafio.Inter.Repository.EmpresaRepositorio;
import com.inter.tulio.Desafio.Inter.Service.impl.EmpresaServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;


@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@Import(EmpresaServiceImpl.class)
@MockBeans({ @MockBean(EmpresaServiceImpl.class), @MockBean(EmpresaRepositorio.class) })
public class EmpresaServiceImplTeste {

    @MockBean
    private EmpresaRepositorio empresaRepositorio = mock(EmpresaRepositorio.class);
    @Autowired
    private EmpresaServiceImpl serviceImp = new EmpresaServiceImpl(empresaRepositorio);


    @Test
    public void buscarPorIdSucessoTeste() {
        int Id = 1;

        Empresa empresaMock = new Empresa();
        empresaMock.setEmpresaId((long)1);
        empresaMock.setAtiva(1);

        Mockito.when(empresaRepositorio.findById((long)Id)).thenReturn(java.util.Optional.of(new Empresa()));
        Mockito.when(empresaRepositorio.getById((long)Id)).thenReturn(empresaMock);
        Empresa empresa = serviceImp.obterEmpresaPorId(Id);

        assertNotNull(empresa);
        assertEquals("1", empresa.getEmpresaId().toString());
        Mockito.verify(empresaRepositorio, times(1)).findById((long)Id);
        Mockito.verify(empresaRepositorio, times(1)).getById((long)Id);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void buscarPorIdInexistenteTeste() {
        int Id = 1;

        Empresa empresaMock = new Empresa();
        empresaMock.setEmpresaId((long)1000);
        empresaMock.setAtiva(1);

        Mockito.when(empresaRepositorio.findById((long)Id)).thenReturn(Optional.empty());
        Mockito.when(empresaRepositorio.getById((long)Id)).thenReturn(empresaMock);
        Empresa empresa = serviceImp.obterEmpresaPorId(Id);

        assertNull(empresa);
        Mockito.verify(empresaRepositorio, times(1)).findById((long)Id);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void buscarPorStatusAtivoTeste() {
        int status = 1;
        List<Empresa> listEmpresaMock = new ArrayList<Empresa>();
        Empresa empresaMock = new Empresa();
        empresaMock.setEmpresaId((long)1);
        empresaMock.setAtiva(status);
        listEmpresaMock.add(empresaMock);

        Mockito.when(empresaRepositorio.findByAtiva(status)).thenReturn(listEmpresaMock);

        List<Empresa> empresasAtivas = serviceImp.obterEmpresaPorStatus(status);

        assertNotNull(empresasAtivas);
        assertEquals(1, empresasAtivas.size());
        assertTrue(empresasAtivas.stream().findFirst().get().getAtiva() == 1);
        Mockito.verify(empresaRepositorio, times(1)).findByAtiva(status);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }


    @Test
    public void buscarPorStatusInativoTeste() {
        int status = 0;
        List<Empresa> listEmpresaMock = new ArrayList<Empresa>();
        Empresa empresaMock = new Empresa();
        empresaMock.setEmpresaId((long)1);
        empresaMock.setAtiva(status);
        listEmpresaMock.add(empresaMock);

        Mockito.when(empresaRepositorio.findByAtiva(status)).thenReturn(listEmpresaMock);

        List<Empresa> empresasAtivas = serviceImp.obterEmpresaPorStatus(status);

        assertNotNull(empresasAtivas);
        assertEquals(1, empresasAtivas.size());
        assertTrue(empresasAtivas.stream().findFirst().get().getAtiva() == 0);
        Mockito.verify(empresaRepositorio, times(1)).findByAtiva(status);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void buscarPorStatusInvalidoTeste() {
        int status = 255;

        List<Empresa> empresasAtivas = serviceImp.obterEmpresaPorStatus(status);

        assertNull(empresasAtivas);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void alterarValorEmpresaSucessoTeste() {
        double novoValor = 35;
        long id = 1;

        serviceImp.atualizarPrecoAcaoEmpresaPorId(id, novoValor);

        Mockito.verify(empresaRepositorio, times(1)).atualizarPrecoAcao(id, novoValor);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void alterarValorEmpresaErroValorInvalidoTeste() {
        double novoValor = -35;
        long id = 1;

        serviceImp.atualizarPrecoAcaoEmpresaPorId(id, novoValor);

        Mockito.verify(empresaRepositorio, times(0)).atualizarPrecoAcao(id, novoValor);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void alterarStatusEmpresaSucessoTeste() {
        int status = 1;
        long id = 1;

        serviceImp.atualizarStatusEmpresaPorId(id, status);

        Mockito.verify(empresaRepositorio, times(1)).atualizarStatusEmpresa(id, status);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }

    @Test
    public void alterarStatusEmpresaErroValorInvalidoTeste() {
        int status = 10;
        long id = 1;

        serviceImp.atualizarStatusEmpresaPorId(id, status);

        Mockito.verify(empresaRepositorio, times(0)).atualizarStatusEmpresa(id, status);
        Mockito.verifyNoMoreInteractions(empresaRepositorio);
    }
}
