# Desafio Inter

## 🏗 O que foi feito?

- Foi desafio foi construido utilizando Spring Boot e base de dados H2 em memória,

# 🖥 Como rodar
- Pasta Desafio-Inter contem projeto, abrir no IntelliJ a pasta interna "Desafio-Inter"
- Buildar/ Rodar a solução.
- A aplicação foi construida, e executada pelo IntelliJ, quando em execução:
- A Documentação da API pode ser econtrara na URL: http://localhost:8080/swagger-ui/
- Para testes manuais, executei as chamadas as API's atravez do Postman, e Swagger
- Testes Unitarios podem ser, e foram executados pela interface do IntelliJ
- Ja existe um script que popula os dados das Acoes/Empresas quando a aplicação é iniciada

# 🎁 Extra

- Se encontra no repositorio também a coleçao de requisiçoes utilizada no postman
- Rotas que exigem CPF, passar string com cpf sem pontuação, length == 11

